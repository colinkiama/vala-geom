using Geometry2D;

int main (string[] args) {
	var p0 = new Point2D (1, 1);
	var p1 = new Point2D (2, 0);
	var p2 = new Point2D (4, 3);
	var p3 = new Point2D (2, 2);

	var l0 = new Line2D (1, 0); // y = x
	var l1 = new Line2D (-1, 2); // y = 2 - x
	var l2 = new Line2D (0.5f, 1); // y = 0.5x + 1
	var l3 = new Line2D.from_points (p0, p1);
	var l4 = new Line2D.from_points (p2, p3); 
	
	var i0 = l1.intersection_with (l0);
	var i1 = l0.intersection_with (l2);
	
	l0.print ("line");
	i0.print ("l0-l1");
	i1.print ("l0-l2");
	l3.print ("p0-p1");
	l4.print ("p3-p2");
	
	return 0;
}
