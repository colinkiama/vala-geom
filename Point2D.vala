// TODO point-line collision

namespace Geometry2D {
	/**
	 * A class which represents a point in two-dimensional Euclidean space based
	 * on computer screen coordinates.
	 */
	public class Point2D : Object {
		/** The x component of the point. **/
		public float x;
		/** The y component of the point. **/
		public float y;

		/**
		 * Create a point (x, y) given x and y.
		 *
		 * @param x The x component of the point.
		 * @param y The y component of the point.
		 */
		public Point2D (float x, float y) {
			this.x = x;
			this.y = y;
		}

		/**
		 * Prints the line to stdout.
		 *
		 * @param name A string which is printed along with the point to identify it
		 */
		public void print (string name = "P") {
			stdout.printf ("%s(%f, %f)\n", name, x, y);
		}
		
		/**
		 * Determines whether this point is inside the given circle.
		 *
		 * @param circ The circle to check whether or not this is inside of
		 * @return true if inside circle, false if not
		 */
		public bool is_inside_circle (Circle2D circ) {
			return Distance.euclidean (this, circ.center) <= circ.radius;
		}
		
		/**
		 * Determines whether this point is inside the given rectangle.
		 *
		 * @param rect The rectangle to check whether or not this is inside of
		 * @return true if inside rectangle, false if not.
		 */
		public bool is_inside_rectangle (Rectangle2D rect) {
			return (x >= rect.topLeft.x &&
			        y >= rect.topLeft.y &&
			        x <= rect.bottomRight.x &&
			        y <= rect.bottomRight.y);
		}
		
		/**
		 * Determines whether this point is inside the given rhombus
		 *
		 * @param rhom The rhombus to check whether or not this is inside of
		 * @return true if inside rhombus, false if not
		 */
		public bool is_inside_rhombus (Rhombus2D rhom) {
			return Distance.manhatten (this, rhom.center) < rhom.radius;
		}
	}
}
