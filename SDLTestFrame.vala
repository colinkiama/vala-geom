using SDL;
using Geometry2D;

/**
 * A framwork for testing classes in the Geometry2D namespace with SDL.
 */
public class SDLTestFrame : Object {
	private unowned Screen screen; // the screen to which we will be drawing
	private bool running = true; // if false the program stops running
	private FPSTimer fpsTimer = new FPSTimer (); // helps keep track of FPS
	private Point2D mousePoint = new Point2D (0, 0); // where the mouse is

	/** Init SDL, inits the framework, and starts the main loop **/
	public SDLTestFrame () {
		screen = Screen.set_video_mode (640, 480, 32, SurfaceFlag.SWSURFACE);
		assert (screen != null);
		
		SDLRenderer.screen = screen;
		
		run ();
	}

	/** The main program loop for updating, rendering, determining FPS **/
	private void run () {
		fpsTimer.start ();
		
		while (running) {
			screen.fill (null, 0); // blank the screen
		
			
			process_events ();

			mouseRect.move_to_point (mousePoint);

			// draw routine
			if (mousePoint.is_inside_rectangle (rect)) {
				SDLRenderer.draw_rectangle (rect, 255, 0, 0);
			} else {
				SDLRenderer.draw_rectangle (rect, 255, 128, 0);
			}
			SDLRenderer.draw_rectangle (mouseRect, 0, 255, 255);
			screen.flip ();

			fpsTimer.increment_counter (); // unnecessary?
			fpsTimer.calculate_fps (true);
		}
		
		fpsTimer.stop ();
	}

}
