/**
 * A class which limits the frames per second, or FPS, of a method which one
 * wants to be looped, to a specified limit.
 */
public class FPSLimiter : Object {
	private double lastSecond = 0.0; // the last second which passed
	private int ticks = 0; // the current frame count
	private int fps = 0; // the actual fps achieved
	
	private bool running = true; // whether to keep looping
	private Timer timer = new Timer (); // keeps track of seconds passed
	
	private int limit; // the upper bound on FPS
	private unowned LoopMethod loop; // the loop to limit the fps of
	
	/** A delegate which represents the function which will be looped **/
	public delegate void LoopMethod ();
	
	/**
	 * Creates, but does not start, a new FPS limiter.
	 *
	 * @param limit The upper bound for the frames per second
	 */
	public FPSLimiter (int limit) {
		this.limit = limit;
	}
	
	/**
	 * Starts the FPS limiter, which will repeatedly call the given loop method
	 * for the specified number of times per second.
	 *
	 * @param loop The method to be looped
	 */
	public void start (LoopMethod loop) {
		this.loop = loop;
		ticks = 0;
		lastSecond = 0;
		run ();
	}
	
	/** Regulates FPS, loops the loop function **/
	private void run () {
		double interval = 1.0 / limit; // define the time between ticks
		double time = 0.0;
		timer.start ();
		
		while (running) {
			if (time >= lastSecond + interval * ticks) {
				loop (); // call the loop method
				ticks++;
			}
			
			if (time >= lastSecond + 1.0) { // if one second has passed
				fps = ticks;
				ticks = 0;
				lastSecond += 1.0;
				stdout.printf ("FPS: %d\n", fps);
			}
			
			time = timer.elapsed ();
			//stdout.printf ("TIME: %f\n", time);
		}
	}
	
	/**
	 * Stops the FPS limiter, halting the loop.
	 */
	public void stop () {
		timer.stop ();
		running = false;
	}
	
	/**
	 * Get the frames per second over the last second.
	 *
	 * @return An integer representing the FPS
	 */
	public int get_fps () {
		return fps;
	}
}
