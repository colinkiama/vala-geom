// TODO rhombus-circle collision
// TODO rhombus-rectangle collision

namespace Geometry2D {
	/**
	 * A class which defines a very specific type of rhombus in two-dimensonal
	 * Euclidean space based on computer screen coordinates. It obeys the
	 * equation :
	 *
	 * abs (x) + abs (y) = r
	 *
	 * where r is the "radius" of the rhombus or the greatest length from the
	 * center to the edge, and the point (x, y) is the center of the rhombus.
	 */
	public class Rhombus2D : Object {
		/** The center point of the rhombus **/
		public Point2D center;
		/** The "radius" of the rhombus **/
		public float radius;
		
		/**
		 * Creates a rhombus defined by a radius and a center point.
		 *
		 * @param r The "radius" of the rhombus
		 * @param center The center of the rhombus
		 */
		public Rhombus2D (float r, Point2D center) {
			radius = r;
			this.center = center;
			assert (r > 0);
		}
		
		/**
		 * Determines whether this rhombus is touching the given rhombus
		 *
		 * @param rhom The rhombus to check collision with
		 * @return true if this touches the other rhombus, false if not
		 */
		public bool intersect_rhombus (Rhombus2D rhom) {
			float dist = this.radius + rhom.radius;
			return Distance.manhatten (this.center, rhom.center) <= dist;
		}
		

	}
}
