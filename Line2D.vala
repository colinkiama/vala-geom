namespace Geometry2D {
	/**
	 * A class to represent a line of the form y = m*x + b, that is, a line in two-
	 * dimensional Euclidean space.
	 */
	// TODO what about slopes of 0 or inf?
	// TODO what coordinate system is this relative to?
	public class Line2D : Object {
		/** The slope, or m, of the line. **/
		public float slope;
		/** The y-intercept, or b, of the line. **/
		public float yIntercept;
	
		/**
		 * Create a line with slope m and y-intercept b.
		 *
		 * @param m The slope
		 * @param b The y-intercept
		 */
		public Line2D (float m, float b) {
			slope = m;
			yIntercept = b;
		}
	
		/**
		 * Finds the line which is defined by points a and b.
		 *
		 * @param a One point which defines a line
		 * @param b Another point which defines a line
		 */
		public Line2D.from_points (Point2D a, Point2D b) {
			// fun fun linear algebra time!
			slope = (b.y - a.y) / (b.x - a.x);
			yIntercept = b.y - (b.x * slope);
		}
		
		/**
		 * Plugs the given x into the line equation and returns the result (y).
		 *
		 * @param x The x to put into the formula
		 * @return The y of y = m*x + b
		 */
		public float find_y (float x) {
			return slope * x + yIntercept;
		}
	
		/**
		 * Computes the point at which this line intersects with the given line.
		 *
		 * @param line The line to check intersection with
		 * @return Point2D representing where two lines cross, or null if no intersection
		 */
		public Point2D? intersection_with (Line2D line) {
			// moar linear algebra!
			if (this.slope == line.slope) {
				if (this.yIntercept == line.yIntercept) {
					return new Point2D (0, yIntercept); // TODO infinitely many solutions
				} else {
					return null; // NO SOLUTION
				}
			}
		
			float x = (line.yIntercept - this.yIntercept) / (this.slope - line.slope);
			float y = this.slope * x + this.yIntercept;
		
			return new Point2D (x, y);
		}
	
		/**
		 * Prints the line to stdout.
		 *
		 * @param name Optional name to give the line.
		 */
		public void print (string name = "f") {
			if ( yIntercept > 0 ) {
				stdout.printf ("%s(x) = %f*x + %f\n", name, slope, yIntercept);
			} else if ( yIntercept == 0 ) {
				stdout.printf ("%s(x) = %f*x\n", name, slope);
			} else { // yIntercept < 0
				stdout.printf ("%s(x) = %f*x %f\n", name, slope, yIntercept);
			}
		}
	}
}
