// TODO rectangle-circle collision
// TODO rectangle-rhombus collision
// TODO rectangle-line collision
// TODO solve move_to_point fiasco

namespace Geometry2D {
	/**
	 * A class which represents a rectangle in two-dimensional Euclidean space
	 * based on computer screen coordinates.
	 */
	public class Rectangle2D : Object {
		/** The point which defines the top left corner of the rectangle **/
		public Point2D topLeft;
		/** The point which defines the bottom right corner of the rectangle **/
		public Point2D bottomRight;
		
		/**
		 * Define a rectangle given an x, y, width, and height.
		 *
		 * @param x The x coordinate of the top left corner
		 * @param y The y coordinate of the top right corner
		 * @param width The width of the rectangle
		 * @param height The height of the rectangle
		 */
		public Rectangle2D (float x, float y, float width, float height) {
			topLeft = new Point2D (x, y);
			bottomRight = new Point2D (x + width, y + height);
			assert (width > 0 && height > 0);
		}
		
		/**
		 * Define a rectangle given a top-left corner and a bottom-right corner.
		 * 
		 * @param topleft The point representing the top left corner
		 * @param botright The point representing the bottom right corner
		 */
		public Rectangle2D.from_points (Point2D topleft, Point2D botright) {
			topLeft = new Point2D (topleft.x, topleft.y);
			bottomRight = new Point2D (botright.x, botright.y);
			assert (bottomRight.x > topLeft.x && bottomRight.y > topLeft.y);
		}
		
		/**
		 * Returns the width of the rectangle
		 *
		 * @return The width of the rectangle
		 */
		public float get_width () {
			return bottomRight.x - topLeft.x;
		}
		
		/**
		 * Returns the height of the rectangle
		 *
		 * @return The height of the rectangle
		 */
		public float get_height () {
			return bottomRight.y - topLeft.y;
		}
		
		/**
		 * Checks if this rectangle is touching another rectangle.
		 *
		 * @param rect The rectangle to check collision with
		 */
		public bool intersect_rect (Rectangle2D rect) {
			if (this.bottomRight.x < rect.topLeft.x ||
			    this.bottomRight.y < rect.topLeft.y ||
			    this.topLeft.x > rect.bottomRight.x ||
			    this.topLeft.y > rect.bottomRight.y ) {
			    return false;
			}
			return true;
		}
		
		/**
		 * Moves the rectangle to the given point, updating both the top left
		 * and bottom right corners.
		 *
		 * @param p The new spot for the top left corner
		 */
		public void move_to_point (Point2D p) {
			//stdout.printf ("topLeft.x: %f topLeft.y: %f bottomRight.x: %f bottomRight.y: %f\n", topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
			float w = get_width ();
			float h = get_height ();
			topLeft.x = p.x;
			topLeft.y = p.y;
			bottomRight.x = topLeft.x + w;
			bottomRight.y = topLeft.y + h;
		}
	}
}
