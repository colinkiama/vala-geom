/**
 * A class to keep track of frames per second, or FPS. Essentially a wrapper for
 * GLib.Timer.
 */
public class FPSTimer : Object {
	private int ticks = 0;
	private int lastSecond = 0;
	private Timer timer;
	private int fps = 0;
	
	/**
	 * Creates, but does not start, a new FPS timer.
	 */
	public FPSTimer () {
		timer = new Timer ();
	}
	
	/**
	 * Starts the FPS timer, resetting all the values back to the initial.
	 */
	public void start () {
		ticks = 0;
		lastSecond = 0;
		timer.start ();
	}
	
	/**
	 * Stops the FPS timer. The timer can be reset by calling start, or resumed
	 * by calling resume.
	 */
	public void stop () {
		timer.stop ();
	}
	
	/**
	 * Increments the internal tick count used for determining FPS. This must be
	 * called from the main loop to use this class effectively.
	 */
	public void increment_counter () {
		ticks++;
	}
	
	/**
	 * Get the frames per second over the last second.
	 *
	 * @return An integer representing the FPS
	 */
	public int get_fps () {
		return fps;
	}
	
	/**
	 * Checks to see if a second has passed. If so, it calculates the new frames
	 * per second. The new FPS can be obtained with get_fps. This must be called
	 * from the main loop to use this class effectively
	 *
	 * @param print Whether to print the FPS to stdout or not
	 */
	public void calculate_fps (bool print = false) {
		if (timer.elapsed () >= lastSecond + 1) {
			lastSecond++;
			fps = ticks;
			ticks = 0;
			if (print) stdout.printf ("FPS: %d\n", fps);
		}
	}
	
}
