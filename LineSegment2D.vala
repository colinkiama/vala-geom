namespace Geometry2D {
	/**
	 * A line segment is a part of a line defined by two points where the line
	 * only exists between those two points and at those two points, but nowhere
	 * else. This constrasts Line2D, which represents a line stretching to
	 * infinity.
	 */
	public class LineSegment2D : Object {
		/** One endpoint of the line segment **/
		public Point2D start;
		/** Another endpoint of the line segment **/
		public Point2D end;
		
		/**
		 * Creates a line segment given two points to define it.
		 *
		 * @param a The "start" of the line segment.
		 * @param b The "end" of the line segment.
		 */
		public LineSegment2D (Point2D a, Point2D b) {
			start = a;
			end = b;
		}
	}
}
