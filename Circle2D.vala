// TODO circle-rectangle collision
// TODO circle-rhombus collision
// TODO circle-line collision

namespace Geometry2D {
	/**
	 * A class which represents a circle in two-dimensional Euclidean space
	 * based on computer screen coordinates.
	 */
	public class Circle2D : Object {
		/** The radius of the circle. **/
		public float radius;
		/** Where the circle is on the plane. **/
		public Point2D center;
		
		/**
		 * Create a circle with the specified radius at the specified point.
		 *
		 * @param r The radius of the circle
		 * @param center The Point which is the center of the circle
		 */
		public Circle2D (float r, Point2D center) {
			radius = r;
			this.center = center;
			assert (radius > 0);
		}
		
		/**
		 * Determines whether this circle touches the given circle.
		 *
		 * @param circ The circle to check for touching.
		 * @return true if touching, false if not
		 */
		public bool intersect_circle (Circle2D circ) {
			float dist = this.radius + circ.radius;
			
			return Distance.euclidean (this.center, circ.center) <= dist;
		}
	}
}
