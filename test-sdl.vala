using Geometry2D;
using SDL;

// valac --pkg sdl -o test-sdl test-sdl.vala Circle2D.vala Distance.vala LineSegment2D.vala Point2D.vala Rectangle2D.vala Rhombus2D.vala FPSTimer.vala SDLRenderer.vala -X -lm

/** A test of aspects of the Geometry2D namespace with SDL **/
public class TestSDL : Object {
	
	private unowned Screen screen; // the screen to which we will be drawing
	private bool running = true; // if false the program stops running
	private FPSTimer fpsTimer = new FPSTimer (); // helps keep track of FPS
	private Point2D mousePoint = new Point2D (0, 0); // where the mouse is
	
	private Rectangle2D rect = new Rectangle2D (48, 16, 32, 64); // a test rectangle
	private Rectangle2D mouseRect = new Rectangle2D (0, 0, 48, 16); // rect which follows mouse
	
	/** Set up the test and run it **/
	public TestSDL () {
		screen = Screen.set_video_mode (640, 480, 32, SurfaceFlag.SWSURFACE);
		assert (screen != null);
		
		SDLRenderer.screen = screen;
		
		run ();
	}
	
	/** The main program loop for updating, rendering, determining FPS **/
	private void run () {
		fpsTimer.start ();
		
		while (running) {		
			process_events ();
			update ();
			render ();
			
			fpsTimer.increment_counter (); // unnecessary?
			fpsTimer.calculate_fps (true);
		}
		
		fpsTimer.stop ();
	}
	
	/** Checks for user input and deals with it accordingly **/
	private void process_events () {
		Event event;
		while (Event.poll (out event) == 1) { // redundant?
			if (event.type == EventType.QUIT) running = false;
			if (event.type == EventType.MOUSEMOTION) {
				MouseMotionEvent mme = event.motion;
				mousePoint.x = (int)mme.x;
				mousePoint.y = (int)mme.y;
				mouseRect.move_to_point (mousePoint);
			}
		}
	}
	
	/** Perform tick of game logic **/
	private void update () {
	}
	
	/** Perform all drawing operations **/
	private void render () {
		screen.fill (null, 0); // blank the screen
		
		if (mouseRect.intersect_rect (rect)) {
			SDLRenderer.draw_rectangle (rect, 255, 0, 0);
		} else {
			SDLRenderer.draw_rectangle (rect, 255, 128, 0);
		}
		
		SDLRenderer.draw_rectangle (mouseRect, 0, 255, 255);
		
		screen.flip ();
	}
	
	/** Inits SDL, runs the program, then quits SDL and terminates **/
	public static int main (string[] args) {
		SDL.init ();
	
		new TestSDL ();
	
		SDL.quit ();
		return 0;
	}
	
}


