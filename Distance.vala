using GLib.Math;

namespace Geometry2D {
	/**
	 * A class which provides class methods for determining distances.
	 */
	public class Distance : Object {
		
		// prevent instantiation
		private Distance () {	
		}
		
		/**
		 * Computes the Euclidean distance between two points.
		 *
		 * @param a A point
		 * @param b Another point
		 * @return sqrt (sqr (a.x - b.x) + sqr (a.y - b.y)) 
		 */
		public static double euclidean (Point2D a, Point2D b) {
			float s = a.x - b.x;
			float t = a.y - b.y;
			return sqrt ((s * s) + (t * t));
		}
		
		/**
		 * Computes the Manhatten distance between two points.
		 *
		 * @param a A point
		 * @param b Another point
		 * @return abs (a.x - b.x) + abs (a.y - b.y)
		 */
		public static double manhatten (Point2D a, Point2D b) {
			return fabs (a.x - b.y) + fabs (a.y - b.y);
		}
		
	}
}
