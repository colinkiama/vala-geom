using SDL;
// TODO use SDLGraphics

namespace Geometry2D {
	/**
	 * A class which provides class methods with define behaviour for drawing
	 * various shapes defined in the Geometry2D namespace with SDL.
	 */
	public class SDLRenderer : Object {
		// INSERT STATIC VARIABLES HERE
		/** The screen to which the shapes will be drawn to. **/
		public static unowned Screen screen = null;
		
		// prevents instantiaon
		private SDLRenderer () {
		}
		
		/**
		 * Draws a rectangle to the screen with the given color.
		 *
		 * @param rect The rectangle to draw
		 * @param color The color to use when drawing
		 */
		public static void draw_rectangle (Rectangle2D rect, uchar r, uchar g, uchar b) {
			assert (screen != null);
			
			var sdlrect = Rect () {
				x = (int16)rect.topLeft.x,
				y = (int16)rect.topLeft.y,
				w = (uint16)rect.get_width (),
				h = (uint16)rect.get_height ()
			};
			
			uint32 color = screen.format.map_rgb (r, g, b);
			
			screen.fill (sdlrect, color);
		}
	}
}
